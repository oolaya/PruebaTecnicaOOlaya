namespace PPTOOlayaApis
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
            Database.SetInitializer<Context>(new CreateDatabaseIfNotExists<Context>());
        }

        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<Agendum> Agenda { get; set; }
        public virtual DbSet<CitasMedica> CitasMedicas { get; set; }
        public virtual DbSet<DisponibilidadMedico> DisponibilidadMedicos { get; set; }
        public virtual DbSet<EstadoCita> EstadoCitas { get; set; }
        public virtual DbSet<EstadosAgenda> EstadosAgendas { get; set; }
        public virtual DbSet<FranjaHoraria> FranjaHorarias { get; set; }
        public virtual DbSet<HorasAtencionCita> HorasAtencionCitas { get; set; }
        public virtual DbSet<LogAgenda> LogAgendas { get; set; }
        public virtual DbSet<LogCitasMedica> LogCitasMedicas { get; set; }
        public virtual DbSet<LogErrore> LogErrores { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
