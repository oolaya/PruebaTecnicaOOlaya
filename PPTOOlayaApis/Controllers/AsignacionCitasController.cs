﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PPTOOlayaApis.Controllers
{
    public class AsignacionCitasController : ApiController
    {
        private IAsignacionCitas _IAsignacionCitas = (IAsignacionCitas)new AsignacionCitasRepository();
        /// <summary>
        /// Metodo GET recibe filto para taer diferentes consultas
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        [EnableCors(origins: "*", headers: "*", methods: "*")]

        // GET: api/AsignacionCitas
        public IHttpActionResult Get(string filtro)
        {
            switch (filtro)
            {
                case "GetAllCitas":
                    return Ok(_IAsignacionCitas.GerAllCitas());
                default:
                    break;
            }
            return Ok(400);
        }

        // GET: api/AsignacionCitas/5
        /// <summary>
        /// Retorna consultas por paciente
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IHttpActionResult Get(int id)
        {
            return Ok(_IAsignacionCitas.GerAllCitasXIdPaciente(id));
        }

        // POST: api/AsignacionCitas
        /// <summary>
        /// Permite asignar citas POST
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IHttpActionResult Post(CitasMedica model)
        {
            return Ok(_IAsignacionCitas.AsiganacionCitas(model));
        }

        // PUT: api/AsignacionCitas/5
        /// <summary>
        /// Put Update Citas medicas
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <param name="IdEstado"></param>
        /// <param name="IdCita"></param>
        /// <param name="IdusuarioRegistra"></param>
        /// <returns></returns>
        public IHttpActionResult Put(string IdEstado, int IdCita, string IdusuarioRegistra)
        {
            return Ok(_IAsignacionCitas.UpdateCitaMecica(IdEstado, IdCita, IdusuarioRegistra));
        }
    }
}
