﻿using PPTOOlayaApis.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Controllers.Utils
{
    public static class LogErroresSistema
    {
        static Context _context = new Context();

        /// <summary>
        /// Creo metodo generico para la insercion de log para errores del sistema. 
        /// oolay 26/04/2017
        /// </summary>
        /// <param name="e"></param>
        public static void CrearLog(Exception ex, string Comentarios = "")
        {
            var st = new StackTrace(ex, true);

            var informationError = st.GetFrames()
                          .Select(frame => new
                          {
                              FileName = frame.GetFileName(),
                              LineNumber = frame.GetFileLineNumber(),
                              ColumnNumber = frame.GetFileColumnNumber(),
                              Method = frame.GetMethod(),
                              Class = frame.GetMethod().DeclaringType,
                          }).FirstOrDefault();

            LogErrore log = new LogErrore
            {
                FechaLogErrores = DateTime.Now,
                HelpLink = (ex.HelpLink == null) ? "" : ex.HelpLink.ToString(),
                HResult = (ex.HResult == 0) ? "" : ex.HResult.ToString(),
                InnerException = (ex.InnerException == null) ? "" : ex.InnerException.ToString(),
                MensajeLogError = ex.Message.ToString(),
                ClaseLogError = informationError.Class.ToString(),
                MetodoLogError = informationError.Method.ToString(),
                LineaLogError = informationError.LineNumber.ToString(),
                Source = ex.Source.ToString(),
                StackTrace = ex.StackTrace.ToString(),
                TargetSite = ex.StackTrace.ToString(),
                ComentarioDesarrollador = Comentarios
            };

            InsertLog(log);

        }

        /// <summary>
        /// Inserto el log en la tabla. 
        /// oolaya 26/04/2017
        /// </summary>
        /// <param name="Log"></param>
        private static void InsertLog(LogErrore Log)
        {
            try
            {
                _context.LogErrores.Add(Log);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}