﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTOOlayaApis.Controllers.Interfaces
{
    interface IAsignacionCitas
    {
        dynamic GerAllCitas();
        dynamic GerAllCitasXIdPaciente(int IdPaciente);
        dynamic AsiganacionCitas(CitasMedica model);
        dynamic UpdateCitaMecica(string IdEstado, int IdCita, string IdusuarioRegistra);
    }
}
