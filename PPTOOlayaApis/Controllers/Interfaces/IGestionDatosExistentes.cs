﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTOOlayaApis.Controllers.Interfaces
{
    interface IGestionDatosExistentes
    {
        dynamic GetAllDoctors();
        dynamic GetDoctorsId(string id);
        dynamic GetAllpatients();
        dynamic GetpatientsId(string id);
        dynamic GetAllspecialties();
        dynamic GetspecialtiesId(string id);
    }
}
