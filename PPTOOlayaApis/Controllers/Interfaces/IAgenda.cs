﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTOOlayaApis.Controllers.Interfaces
{
    interface IAgenda
    {
        dynamic GetAllAgenda();
        dynamic GetAgendaDisponibleXFecha(string fechaInicial, string fechaFinal);
        dynamic GetAgendaXFecha(string fechaInicial, string fechaFinal);
        dynamic AgendaUtomatica(string idUsuarioRegistra);
    }
}
