﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPTOOlayaApis.Controllers.Interfaces
{
    interface IDisponibilidadMedico
    {
        dynamic InsertDisponibilidadMedico(List<DisponibilidadMedico> item);
        dynamic ConsultarTiemposMedicos();
    }
}
