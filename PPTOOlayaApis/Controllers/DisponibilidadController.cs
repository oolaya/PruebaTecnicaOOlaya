﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PPTOOlayaApis.Controllers
{
    public class DisponibilidadMedicoController : ApiController
    {
        IDisponibilidadMedico _Idisponibilidad = (IDisponibilidadMedico)new DisponibilidadMedicoRepository();
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        /// <summary>
        /// Consulta tiempos de medicos
        /// OOlaya 
        /// 27/04/2018
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        // GET: api/DisponibilidadMedico
        public IHttpActionResult Get(string filtro)
        {
            switch (filtro)
            {
                case "ConsultarTiemposMedicos":
                    return Ok(_Idisponibilidad.ConsultarTiemposMedicos());
                default:
                    return Ok(400);
            }
        }


        // POST: api/DisponibilidadMedico
        /// <summary>
        /// Crea disponibilidad de medicos
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public IHttpActionResult Post(List<DisponibilidadMedico> item)
        {
            if (!ModelState.IsValid)
            {
                return Ok(HttpStatusCode.BadRequest);
            }
            return Ok(_Idisponibilidad.InsertDisponibilidadMedico(item));
        }
    }
}
