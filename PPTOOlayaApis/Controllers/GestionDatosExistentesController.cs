﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PPTOOlayaApis.Controllers
{
    public class GestionDatosExistentesController : ApiController
    {

        IGestionDatosExistentes _GestionDatos = (IGestionDatosExistentes)new GestionDatosExistentesRepository();

        [EnableCors(origins: "*", headers: "*", methods: "*")]
        // GET: api/GestionDatosExistentes
        /// <summary>
        /// Recibe filtros para get's de gestion de informacion existente
        /// </summary>
        /// <param name="filtro">Recibe filtro que permite regresar la consulta</param>
        /// <param name="id">Identificador de acuerdo al filtro</param>
        /// <returns></returns>
        public IHttpActionResult Get(string filtro = "", string ident = "")
        {
            switch (filtro)
            {
                case "GetAllDoctors":
                    return Ok(_GestionDatos.GetAllDoctors());
                case "GetDoctorsId":
                    return Ok(_GestionDatos.GetDoctorsId(ident));
                case "GetAllpatients":
                    return Ok(_GestionDatos.GetAllpatients());
                case "GetpatientsId":
                    return Ok(_GestionDatos.GetpatientsId(ident));
                case "GetAllspecialties":
                    return Ok(_GestionDatos.GetAllspecialties());
                case "GetspecialtiesId":
                    return Ok(_GestionDatos.GetspecialtiesId(ident));
                default:
                    return Ok(204);
            }
        }
    }
}
