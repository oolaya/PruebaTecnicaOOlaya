﻿using PPTOOlayaApis.Controllers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class AgendaController : ApiController
    {
        private IAgenda _IAgenda = (IAgenda)new AgendaRepository();
        // GET: api/Agenda
        /// <summary>
        /// Get recibe filtros y parametros obcionales
        /// </summary>
        /// <param name="filtro"></param>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        public IHttpActionResult Get(string filtro, string fechaInicial = "", string fechaFinal = "")
        {
            switch (filtro)
            {
                case "GetAllAgenda":
                    return Ok(_IAgenda.GetAllAgenda());
                case "GetAgendaDisponibleXFecha":
                    return Ok(_IAgenda.GetAgendaDisponibleXFecha(fechaInicial, fechaFinal));
                case "GetAgendaXFecha":
                    return Ok(_IAgenda.GetAgendaXFecha(fechaInicial, fechaFinal));
                default:
                    break;
            }
            return Ok(200);
        }


        /// <summary>
        /// POST permite crear agenda automatica
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="filtro"></param>
        /// <param name="idUsuarioRegistra"></param>
        /// <returns></returns>
        public IHttpActionResult Post(string filtro, string idUsuarioRegistra)
        {
            switch (filtro)
            {
                case "AgendaAutomatica":
                    return Ok(_IAgenda.AgendaUtomatica(idUsuarioRegistra));
                default:
                    return Ok(400);
            }
        }
    }
}
