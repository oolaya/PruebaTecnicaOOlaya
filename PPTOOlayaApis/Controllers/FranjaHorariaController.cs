﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace PPTOOlayaApis.Controllers
{
    public class FranjaHorariaController : ApiController
    {
        IFranjaHoraria _Ifranja = (IFranjaHoraria)new FranjaHorariaRepository();
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        /// <summary>
        /// Get Franja Horaria 
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <param name="filtro"></param>
        /// <returns></returns>
        // GET: api/FranjaHoraria
        public IHttpActionResult Get(string filtro = "")
        {
            switch (filtro)
            {
                case "GetAllFranjaHoraria":
                    return Ok(_Ifranja.GetAllFranjaHoraria());
                default:
                    return Ok(201);
            }
        }
    }
}
