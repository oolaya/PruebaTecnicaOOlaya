﻿using PPTOOlayaApis.Controllers.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PPTOOlayaApis.Models;
using PPTOOlayaApis.Controllers.Utils;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class AgendaRepository : IAgenda
    {
        /// <summary>
        /// Metodos GET Agenda
        /// OOLaya 
        /// 28/04/2017
        /// </summary>
        /// <returns></returns>
        #region
        public dynamic GetAllAgenda()
        {
            Context _Context = new Context();
            GestionDatosExistentesRepository _med = new GestionDatosExistentesRepository();
            var ConsultaAgenda = _Context.Agenda.Select(a => a).ToList();
            var ListMedicos = _med.GetAllDoctors() as List<Doctors>;
            var joined = ConsultaAgenda.Join(ListMedicos,
                                                     d => d.DisponibilidadMedico.IdMedico,
                                                     m => m.id,
                                                     (d, m) => new
                                                     {
                                                         IdAgenda = d.IdAgenda,
                                                         Hora = d.HoraAgenda,
                                                         Fecha = d.FechaAgenda,
                                                         Franja = d.DisponibilidadMedico.FranjaHoraria.SiglaFranjaHoraria,
                                                         Estado = d.EstadosAgenda.NombreEstado,
                                                         IdMedico = d.DisponibilidadMedico.IdMedico,
                                                         NombreMedico = m.last_name + " " + m.first_name,
                                                         Especialidad = m.specialty_field.specialty_type,
                                                         Idespecialidad = m.specialty_field.id
                                                     }).ToList();
            return joined;
        }

        /// <summary>
        /// Retorna Agenda disponble por rango de fecha 
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        public dynamic GetAgendaDisponibleXFecha(string fechaInicial, string fechaFinal)
        {
            Context _Context = new Context();
            DateTime fecIni = Convert.ToDateTime(fechaInicial);
            DateTime fecfin = Convert.ToDateTime(fechaFinal);
            var ConsultaAgenda = _Context.Agenda.Where(a => (new string[] { "DISPONIBLE" }).Contains(a.EstadosAgenda.NombreEstado)
                                                             && a.FechaAgenda >= fecIni
                                                             && a.FechaAgenda <= fecfin).ToList();
            if (ConsultaAgenda.Count == 0)
            {
                return 204;
            }
            return ConsultaAgenda;
        }

        /// <summary>
        /// Retorna el total de las agendas en rango de fecha sin filro de estado
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <param name="fechaInicial"></param>
        /// <param name="fechaFinal"></param>
        /// <returns></returns>
        public dynamic GetAgendaXFecha(string fechaInicial, string fechaFinal)
        {
            Context _Context = new Context();
            try
            {
                DateTime fecIni = Convert.ToDateTime(fechaInicial);
                DateTime fecfin = Convert.ToDateTime(fechaFinal);
                var ConsultaAgenda = _Context.Agenda.Where(a => a.FechaAgenda >= fecIni
                                                           && a.FechaAgenda <= fecfin).ToList();
                if (ConsultaAgenda.Count == 0)
                {
                    return 204;
                }
                return ConsultaAgenda;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }

        }
        #endregion

        /// <summary>
        /// Metodos POST Agenda 
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <returns></returns>
        #region
        public dynamic AgendaUtomatica(string IdUsuarioRegistra)
        {
            Context _context = new Context();
            GestionDatosExistentesRepository _Medi = new GestionDatosExistentesRepository();
            DateTime now = DateTime.Now;
            try
            {
                var idMedicos = _Medi.GetAllDoctors() as List<Doctors>;
                int[] ArrayIdMed = idMedicos.Select(i => i.id).ToArray();
                var list = _context.DisponibilidadMedicos.Where(d => ArrayIdMed.Contains(d.IdMedico)).ToList();
                var HorasAtencion = _context.HorasAtencionCitas.Select(h => h).ToList();
                var exitenciaAgenda = _context.Agenda.Select(e => e).ToList();
                var estadoAgenda = _context.EstadosAgendas.Where(e => (new string[] { "DISPONIBLE" }).Contains(e.NombreEstado)).FirstOrDefault().IdEstado;
                var joined = list.Join(HorasAtencion,
                                                      d => d.IdFranjaHoraria,
                                                      h => h.IdFranjaHoraria,
                                                      (d, h) => new
                                                      {
                                                          d.IdDisponibilidad,
                                                          d.FranjaHoraria,
                                                          d.IdMedico,
                                                          d.FechaDisponibilidad,
                                                          h.IdHorasAtencion,
                                                          h.HorasAtencion
                                                      }).ToList();
                foreach (var item in joined)
                {
                    var ExisteAgenda = exitenciaAgenda.Where(e => e.DisponibilidadMedico.IdMedico == item.IdMedico && e.FechaAgenda == item.FechaDisponibilidad).FirstOrDefault();
                    if (ExisteAgenda == null)
                    {
                        _context.Agenda.Add(new Agendum
                        {
                            FechaAgenda = item.FechaDisponibilidad,
                            HoraAgenda = item.HorasAtencion,
                            UsurioRegistra = IdUsuarioRegistra,
                            FechaRegistro = now,
                            IdEstado = estadoAgenda,
                            IdDisponibilidad = item.IdDisponibilidad
                        });
                        _context.SaveChanges();
                    }
                }
                return 200;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e, "Error en la creacion automatica de agenda");
                return 400;
            }

        }
        #endregion
    }
}