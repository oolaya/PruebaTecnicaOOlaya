﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Transactions;
using PPTOOlayaApis.Models;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class DisponibilidadMedicoRepository : IDisponibilidadMedico
    {
        #region
        /// <summary>
        /// Consulta Tiempos y agendas por medico 
        /// OOlya 
        /// 28/04/2017
        /// </summary>
        /// <returns></returns>
        public dynamic ConsultarTiemposMedicos()
        {
            Context _context = new Context();
            GestionDatosExistentesRepository _med = new GestionDatosExistentesRepository();
            var _Citasmed = _context.CitasMedicas.Where(d => (new string[] { "ASIGNADA" }).Contains(d.EstadoCita.NombreEstado)).Select(d => d).ToList();

            var ConsultaAgenda = _context.Agenda.Select(a => a).ToList();
            var ListMedicos = _med.GetAllDoctors() as List<Doctors>;
            var joined = ConsultaAgenda.Join(ListMedicos,
                                                     d => d.DisponibilidadMedico.IdMedico,
                                                     m => m.id,
                                                     (d, m) => new
                                                     {
                                                         IdAgenda = d.IdAgenda,
                                                         Hora = d.HoraAgenda,
                                                         Fecha = d.FechaAgenda,
                                                         Franja = d.DisponibilidadMedico.FranjaHoraria.SiglaFranjaHoraria,
                                                         Estado = d.EstadosAgenda.NombreEstado,
                                                         IdMedico = d.DisponibilidadMedico.IdMedico,
                                                         NombreMedico = m.last_name + " " + m.first_name,
                                                         Especialidad = m.specialty_field.specialty_type,
                                                         Idespecialidad = m.specialty_field.id
                                                     }).ToList();
            var ListFinal = joined.GroupJoin(_Citasmed,
                                              foo => foo.IdAgenda,
                                              bar => bar.IdAgenda,
                                              (x, y) => new { Foo = x, Bars = y })
                                                    .SelectMany(
                                              x => x.Bars.DefaultIfEmpty(),
                                              (x, y) => new { Foo = x.Foo, Bar = y });
            return ListFinal;
        }

        #endregion


        /// <summary>
        /// Metodo consumido por POST inserta disponibilidad de medicos
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="Items"></param>
        /// <returns></returns>
        public dynamic InsertDisponibilidadMedico(List<DisponibilidadMedico> Items)
        {
            Context _context = new Context();
            List<TypeError> _listError = new List<TypeError>();
            GestionDatosExistentesRepository _Med = new GestionDatosExistentesRepository();
            try
            {
                using (TransactionScope ts = new TransactionScope())
                {
                    foreach (var item in Items)
                    {
                        var preConsultaDispo = _context.DisponibilidadMedicos.Where(d => d.FechaDisponibilidad == item.FechaDisponibilidad && d.IdMedico == item.IdMedico).FirstOrDefault();
                        if (preConsultaDispo == null)
                        {
                            _context.DisponibilidadMedicos.Add(item);
                            Doctors Medico = _Med.GetDoctorsId(item.IdMedico.ToString());
                            _listError.Add(new TypeError
                            {
                                IdError = 201,
                                MombreError = "Created"
                            });
                        }
                        else
                        {
                            Doctors Medico = _Med.GetDoctorsId(item.IdMedico.ToString());
                            _listError.Add(new TypeError
                            {
                                IdError = 422,
                                MombreError = "Registro existente",
                                DescripcionError = "El usuario " + Medico.identification + " ya tiene Disponibilidad resgistrada para la fecha"
                            });
                        }

                    }
                    _context.SaveChanges();
                    ts.Complete();
                }
                foreach (var item in _listError)
                {
                    if (item.IdError != 201)
                    {
                        return _listError;
                    }
                }
                return 201;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }

        }
    }

}