﻿using PPTOOlayaApis.Controllers.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class LogAgendaRepository
    {
        /// <summary>
        /// Metodo que crea el log historico de las agendas
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="Model"></param>
        public void crearLogAgenda(Agendum Model)
        {
            LogAgenda _logcitas = new LogAgenda();
            Context _context = new Context();
            try
            {
                _logcitas.IdAgenda = Model.IdAgenda;
                _logcitas.IdEstado = Model.IdEstado;
                _logcitas.IdUsuarioRegistra = Model.UsurioRegistra;
                _logcitas.FechaRegistra = DateTime.Now;
                _context.LogAgendas.Add(_logcitas);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e, "Creacion logAgendas medicas");
            }
        }
    }
}