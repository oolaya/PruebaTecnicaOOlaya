﻿using Newtonsoft.Json;
using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Utils;
using PPTOOlayaApis.Models;
using PPTOOlayaApis.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class GestionDatosExistentesRepository : IGestionDatosExistentes
    {
        private string DoctorsUrl = ConfigurationManager.AppSettings["doctors"];
        private string PatientsUrl = ConfigurationManager.AppSettings["patients"];
        private string SpecialtiesUrl = ConfigurationManager.AppSettings["specialties"];

        /// <summary>
        /// Metodo que obtine desde el api existente todos los medicos
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <returns></returns>
        public dynamic GetAllDoctors()
        {
            try
            {
                List<Doctors> _Doctors = new List<Doctors>();
                MyWebRequest request = new MyWebRequest(DoctorsUrl, "GET");
                var objDoctors = request.GetResponse();
                _Doctors = JsonConvert.DeserializeObject<List<Doctors>>(objDoctors);
                if (_Doctors.Count > 0)
                {
                    return _Doctors;
                }
                return 204;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }
        }

        /// <summary>
        /// Metodo que retorna medico por id
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Objeto Dinamico</returns>
        public dynamic GetDoctorsId(string id)
        {
            try
            {
                Doctors _Doctors = new Doctors();
                string UrlRequest = string.Concat(DoctorsUrl, id);
                MyWebRequest request = new MyWebRequest(UrlRequest, "GET");
                var objDoctors = request.GetResponse();
                _Doctors = JsonConvert.DeserializeObject<Doctors>(objDoctors);
                if (!string.IsNullOrEmpty(_Doctors.identification))
                {
                    return _Doctors;
                }
                return 204;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }
        }

        /// <summary>
        /// Obtiene el listado general de los pacientes existentes
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <returns></returns>
        public dynamic GetAllpatients()
        {
            try
            {
                List<patients> _patients = new List<patients>();
                MyWebRequest request = new MyWebRequest(PatientsUrl, "GET");
                var objPatients = request.GetResponse();
                _patients = JsonConvert.DeserializeObject<List<patients>>(objPatients);
                if (_patients.Count > 0)
                {
                    return _patients;
                }
                return 204;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }
        }

        /// <summary>
        /// Metodo que retorna paciente por id
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public dynamic GetpatientsId(string id)
        {
            try
            {
                patients _Patients = new patients();
                string UrlRequest = string.Concat(PatientsUrl, id);
                MyWebRequest request = new MyWebRequest(UrlRequest, "GET");
                var objDoctors = request.GetResponse();
                _Patients = JsonConvert.DeserializeObject<patients>(objDoctors);
                if (!string.IsNullOrEmpty(_Patients.identification))
                {
                    return _Patients;
                }
                return 204;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }
        }

        /// <summary>
        /// Retorna lista total de especialidades de especialidades
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <returns></returns>
        public dynamic GetAllspecialties()
        {
            try
            {
                List<specialties> _specialties = new List<specialties>();
                MyWebRequest request = new MyWebRequest(SpecialtiesUrl, "GET");
                var objspecialties = request.GetResponse();
                _specialties = JsonConvert.DeserializeObject<List<specialties>>(objspecialties);
                if (_specialties.Count > 0)
                {
                    return _specialties;
                }
                return 204;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }
        }

        /// <summary>
        /// Retorna especialidad por id 
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public dynamic GetspecialtiesId(string id)
        {
            try
            {
                specialties _specialties = new specialties();
                string UrlRequest = string.Concat(SpecialtiesUrl, id);
                MyWebRequest request = new MyWebRequest(UrlRequest, "GET");
                var objspecialties = request.GetResponse();
                _specialties = JsonConvert.DeserializeObject<specialties>(objspecialties);
                if (!string.IsNullOrEmpty(_specialties.specialty_type))
                {
                    return _specialties;
                }
                return 204;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }
        }
    }
}