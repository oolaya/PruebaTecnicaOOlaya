﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Utils;
using PPTOOlayaApis.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class AsignacionCitasRepository : IAsignacionCitas
    {
        #region
        /// <summary>
        /// Retorna al total de citas medias sin filtros
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <returns></returns>
        public dynamic GerAllCitas()
        {
            Context _contex = new Context();
            var citasMedicas = _contex.CitasMedicas.Select(c => c).ToList();
            return citasMedicas;
        }

        /// <summary>
        /// Retorna el total de citas medicas por paciente. Historico
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="idPaciente"></param>
        /// <returns></returns>
        public dynamic GerAllCitasXIdPaciente(int idPaciente)
        {
            Context _contex = new Context();
            var citasMedicas = _contex.CitasMedicas.Where(c => c.IdPaciente == idPaciente).Select(c => c).ToList();
            return citasMedicas;
        }
        #endregion


        #region
        /// <summary>
        /// Metodo consumido por POST para la asignacion de citas
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public dynamic AsiganacionCitas(CitasMedica model)
        {
            Context _contex = new Context();
            TypeError _error = new TypeError();
            LogCitasMedicasRepository _logCitas = new LogCitasMedicasRepository();
            LogAgendaRepository _logAgenda = new LogAgendaRepository();
            try
            {
                model.Agendum = _contex.Agenda.Where(a => a.IdAgenda == model.IdAgenda).FirstOrDefault();
                var TieneConsulta = _contex.CitasMedicas.Where(c =>
                                            c.Agendum.FechaAgenda == c.Agendum.FechaAgenda
                                            && c.Agendum.HoraAgenda == c.Agendum.HoraAgenda
                                            && c.IdPaciente == model.IdPaciente).ToList();
                if (TieneConsulta.Count > 0)
                {
                    _error.IdError = 201;
                    _error.MombreError = "No afectado";
                    _error.DescripcionError = "El usuario ya tiene una cita asignada en la misma hora y fecha";
                    return _error;
                }
                else
                {
                    var CitaDisponible = _contex.Agenda.Where(a => a.IdAgenda == model.Agendum.IdAgenda).Select(e => e.EstadosAgenda.NombreEstado == "DISPONIBLE").FirstOrDefault();
                    if (CitaDisponible != null)
                    {
                        _contex.CitasMedicas.Add(model);
                        _contex.SaveChanges();
                        var UpdateAgenda = (from a in _contex.Agenda
                                            where
                                            a.IdAgenda == model.Agendum.IdAgenda
                                            select a);
                        foreach (var item in UpdateAgenda)
                        {
                            item.IdEstado = _contex.EstadosAgendas.Where(e => e.NombreEstado == "ASIGNADA").Select(e => e.IdEstado).FirstOrDefault();
                        }
                        _contex.SaveChanges();
                        _logCitas.crearLogCitasMedicas(model);
                        _logAgenda.crearLogAgenda((Agendum)UpdateAgenda);
                        _error.IdError = 201;
                        _error.MombreError = "Exitoso";
                        _error.DescripcionError = "Su cita fue asignada exitosamente "
                                                   + " Fecha: " + model.Agendum.FechaAgenda.ToString()
                                                   + " Hora: " + model.Agendum.HoraAgenda;
                        return _error;
                    }
                    else
                    {
                        _error.IdError = 304;
                        _error.MombreError = "No modificada";
                        _error.DescripcionError = "No es posible asignar esta cita ya esta reservada";
                        return _error;
                    }
                }
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e, "Error Creacion de citas");
                _error.IdError = 400;
                _error.MombreError = "Error";
                _error.DescripcionError = "Por favor comuniquese con el administrador";
                return _error;
            }
        }
        #endregion

        #region
        /// <summary>
        /// Metodo consumido por PUT para la modificacion de citas mediccas
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="IdEstado"></param>
        /// <param name="IdCita"></param>
        /// <param name="IdusuarioRegistra"></param>
        /// <returns></returns>
        public dynamic UpdateCitaMecica(string IdEstado, int IdCita, string IdusuarioRegistra)
        {
            Context _contex = new Context();
            TypeError _error = new TypeError();
            LogCitasMedicasRepository _logCitas = new LogCitasMedicasRepository();

            try
            {
                int Idestado = _contex.EstadoCitas.Where(e => e.NombreEstado == IdEstado.ToUpper()).Select(i => i.IdEstado).FirstOrDefault();
                List<CitasMedica> UpdateCita = (from c in _contex.CitasMedicas
                                                where
                                                c.IdCitasMedicas == IdCita
                                                select c).ToList<CitasMedica>();
                foreach (var item in UpdateCita)
                {
                    item.IdEstado = Idestado;
                }
                _contex.SaveChanges();
                UpdateCita.FirstOrDefault().UsurioRegistra = IdusuarioRegistra;
                UpdateCita.FirstOrDefault().FechaRegistro = DateTime.Now;
                _logCitas.crearLogCitasMedicas(UpdateCita.FirstOrDefault());
                if (IdEstado == "CANCELADA")
                {
                    LiberarAgenda(IdCita, IdusuarioRegistra);
                }
                _error.IdError = 201;
                _error.MombreError = "Exitoso";
                _error.DescripcionError = "Su cita fue modificada exitosamente ";
                return _error;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e, "Error Creacion de citas");
                _error.IdError = 400;
                _error.MombreError = "Error";
                _error.DescripcionError = "Por favor comuniquese con el administrador";
                return _error;
            }
        }

        /// <summary>
        /// Metodo que libera agenda cuando hay modificacion.
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <param name="IdCita"></param>
        /// <param name="UsuarioRegistra"></param>
        public void LiberarAgenda(int IdCita, string UsuarioRegistra)
        {
            Context _contex = new Context();
            LogAgendaRepository _logAgenda = new LogAgendaRepository();
            int IdAgenda = _contex.CitasMedicas.Where(c => c.IdCitasMedicas == IdCita).Select(c => c.IdAgenda).FirstOrDefault();
            int IdEstado = _contex.EstadosAgendas.Where(e => e.NombreEstado == "DISPONIBLE").Select(e => e.IdEstado).FirstOrDefault();
            List<Agendum> Agenda = (from a in _contex.Agenda
                                    where
                                    a.IdAgenda == IdAgenda
                                    select a).ToList();
            Agendum logAgenda = Agenda.FirstOrDefault();
            foreach (var item in Agenda)
            {
                item.IdEstado = IdEstado;
            }
            _contex.SaveChanges();
            logAgenda.UsurioRegistra = UsuarioRegistra;
            _logAgenda.crearLogAgenda(logAgenda);
        }
        #endregion
    }
}