﻿using PPTOOlayaApis.Controllers.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class LogCitasMedicasRepository
    {
        /// <summary>
        /// Metodo que crea log citas medias historico.
        /// OOlaya 
        /// 27/04/2017
        /// </summary>
        /// <param name="Model"></param>
        public void crearLogCitasMedicas(CitasMedica Model)
        {
            LogCitasMedica _logcitas = new LogCitasMedica();
            Context _context = new Context();
            try
            {
                _logcitas.IdCitasMedicas = Model.IdCitasMedicas;
                _logcitas.IdEstado = Model.IdEstado;
                _logcitas.IdUsuarioRegistra = Model.UsurioRegistra;
                _logcitas.FechaRegistra = DateTime.Now;
                _context.LogCitasMedicas.Add(_logcitas);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e, "Creacion logcitas medicas");
            }
        }
    }
}