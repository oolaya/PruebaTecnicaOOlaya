﻿using PPTOOlayaApis.Controllers.Interfaces;
using PPTOOlayaApis.Controllers.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Controllers.Repository
{
    public class FranjaHorariaRepository : IFranjaHoraria
    {
        /// <summary>
        /// Obtine la franja horaria existente. 
        /// OOlaya
        /// 27/04/2017
        /// </summary>
        /// <returns></returns>
        public dynamic GetAllFranjaHoraria()
        {
            Context _context = new Context();
            try
            {
                var Franjas = _context.FranjaHorarias.Select(f => f).ToList();
                return Franjas;
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e);
                return 400;
            }

        }
    }
}