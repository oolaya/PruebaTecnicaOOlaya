namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EstadoCita
    {
        public EstadoCita()
        {
            CitasMedicas = new HashSet<CitasMedica>();
            LogCitasMedicas = new HashSet<LogCitasMedica>();
        }

        [Key]
        public int IdEstado { get; set; }

        public string NombreEstado { get; set; }

        public bool Estado { get; set; }

        public virtual ICollection<CitasMedica> CitasMedicas { get; set; }

        public virtual ICollection<LogCitasMedica> LogCitasMedicas { get; set; }
    }
}
