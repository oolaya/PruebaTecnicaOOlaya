namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LogErrore
    {
        [Key]
        public int IdLogErrores { get; set; }

        public DateTime FechaLogErrores { get; set; }

        public string Controlador { get; set; }

        public string ClaseLogError { get; set; }

        public string MetodoLogError { get; set; }

        public string LineaLogError { get; set; }

        public string ColumnaLogError { get; set; }

        public string MensajeLogError { get; set; }

        public string HelpLink { get; set; }

        public string HResult { get; set; }

        public string InnerException { get; set; }

        public string Source { get; set; }

        public string StackTrace { get; set; }

        public string TargetSite { get; set; }

        public string ComentarioDesarrollador { get; set; }
    }
}
