namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DisponibilidadMedicos")]
    public partial class DisponibilidadMedico
    {
        [Key]
        public int IdDisponibilidad { get; set; }

        public int IdMedico { get; set; }

        public int IdFranjaHoraria { get; set; }

        public DateTime FechaDisponibilidad { get; set; }

        public virtual FranjaHoraria FranjaHoraria { get; set; }
    }
}
