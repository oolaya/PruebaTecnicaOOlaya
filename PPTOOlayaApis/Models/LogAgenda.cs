namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LogAgenda
    {
        [Key]
        public int IdLogAgenda { get; set; }

        public int IdAgenda { get; set; }

        public int IdEstado { get; set; }

        public string IdUsuarioRegistra { get; set; }

        public DateTime FechaRegistra { get; set; }

        public virtual Agendum Agendum { get; set; }

        public virtual EstadosAgenda EstadosAgenda { get; set; }
    }
}
