﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Models
{
    public class Doctors
    {
        public int id { get; set; }
        public string url { get; set; }
        public string identification { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string blood_type { get; set; }
        public specialties specialty_field { get; set; }
    }
}

