namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CitasMedica
    {
        public CitasMedica()
        {
            LogCitasMedicas = new HashSet<LogCitasMedica>();
        }

        [Key]
        public int IdCitasMedicas { get; set; }

        public int IdAgenda { get; set; }

        public int IdPaciente { get; set; }

        public string UsurioRegistra { get; set; }

        public DateTime FechaRegistro { get; set; }

        public int IdEstado { get; set; }

        public virtual Agendum Agendum { get; set; }

        public virtual EstadoCita EstadoCita { get; set; }

        public virtual ICollection<LogCitasMedica> LogCitasMedicas { get; set; }
    }
}
