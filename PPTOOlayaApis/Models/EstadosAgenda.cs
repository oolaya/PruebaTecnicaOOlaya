namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class EstadosAgenda
    {
        public EstadosAgenda()
        {
            Agenda = new HashSet<Agendum>();
            LogAgendas = new HashSet<LogAgenda>();
        }

        [Key]
        public int IdEstado { get; set; }

        public string NombreEstado { get; set; }

        public bool Estado { get; set; }

        public virtual ICollection<Agendum> Agenda { get; set; }

        public virtual ICollection<LogAgenda> LogAgendas { get; set; }
    }
}
