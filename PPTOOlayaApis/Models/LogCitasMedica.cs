namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class LogCitasMedica
    {
        [Key]
        public int IdLogCitasMedicas { get; set; }

        public int IdCitasMedicas { get; set; }

        public int IdEstado { get; set; }

        public string IdUsuarioRegistra { get; set; }

        public DateTime FechaRegistra { get; set; }

        public virtual CitasMedica CitasMedica { get; set; }

        public virtual EstadoCita EstadoCita { get; set; }
    }
}
