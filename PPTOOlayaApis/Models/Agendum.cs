namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Agendum
    {
        public Agendum()
        {
            CitasMedicas = new HashSet<CitasMedica>();
            LogAgendas = new HashSet<LogAgenda>();
        }

        [Key]
        public int IdAgenda { get; set; }

        public DateTime FechaAgenda { get; set; }

        public string HoraAgenda { get; set; }

        public string UsurioRegistra { get; set; }

        public DateTime FechaRegistro { get; set; }

        public int IdEstado { get; set; }

        public int IdDisponibilidad { get; set; }

        public virtual DisponibilidadMedico DisponibilidadMedico { get; set; }

        public virtual EstadosAgenda EstadosAgenda { get; set; }

        public virtual ICollection<CitasMedica> CitasMedicas { get; set; }

        public virtual ICollection<LogAgenda> LogAgendas { get; set; }
    }
}
