﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PPTOOlayaApis.Models
{
    public class TypeError
    {
        public int IdError { get; set; }
        public string MombreError { get; set; }
        public string DescripcionError { get; set; }
    }
}