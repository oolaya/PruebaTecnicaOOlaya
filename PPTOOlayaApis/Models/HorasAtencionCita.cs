namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class HorasAtencionCita
    {

        [Key]
        public int IdHorasAtencion { get; set; }

        public string HorasAtencion { get; set; }

        public int IdFranjaHoraria { get; set; }

        [ForeignKey("IdFranjaHoraria")]
        public virtual FranjaHoraria FranjaHoraria { get; set; }
    }
}
