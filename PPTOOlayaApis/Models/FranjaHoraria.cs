namespace PPTOOlayaApis
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FranjaHoraria
    {
        [Key]
        public int IdFranjaHoraria { get; set; }

        public string NombreFranjaHoraria { get; set; }

        public string SiglaFranjaHoraria { get; set; }

        public string RangoHorario { get; set; }

    }
}
