namespace PPTOOlayaApis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Creotablas11 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DisponibilidadMedicos", "FechaDisponibilidad", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
           // DropColumn("dbo.DisponibilidadMedicos", "FechaDisponibilidad");
        }
    }
}
