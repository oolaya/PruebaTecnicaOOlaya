﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PPTOOlayaApis.Controllers;
using PPTOOlayaApis.Models;
using System.Collections.Generic;
using PPTOOlayaApis.Controllers.Utils;
using Newtonsoft.Json;
using System.Web.Http.Results;
using System.Text;
using System.Web.Http;
using System.Net.Http;
using System.Net;
using PPTOOlayaApis.Controllers.Repository;
using PPTOOlayaApis.Models;
using PPTOOlayaApis;
using System.Linq;

namespace PPTOOlayaTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            try
            {
                List<Doctors> _DoctorsTest = new List<Doctors>();
                GestionDatosExistentesRepository _DatosTest = new GestionDatosExistentesRepository();
                _DoctorsTest = _DatosTest.GetAllDoctors() as List<Doctors>;
                Assert.IsNotNull(_DoctorsTest);
            }
            catch (Exception e)
            {
                LogErroresSistema.CrearLog(e, "Error en TestMethod1");
            }

        }


        [TestMethod]
        public void InsertaDisponiblidad()
        {
            List<Doctors> _DoctorsTest = new List<Doctors>();
            GestionDatosExistentesRepository _DatosTest = new GestionDatosExistentesRepository();
            _DoctorsTest = _DatosTest.GetAllDoctors() as List<Doctors>;
            Assert.IsNotNull(_DoctorsTest, "List Medicos Ok");

            List<FranjaHoraria> _ModelFranja = new List<FranjaHoraria>();
            FranjaHorariaRepository _franja = new FranjaHorariaRepository();
            _ModelFranja = _franja.GetAllFranjaHoraria() as List<FranjaHoraria>;
            Assert.IsNotNull(_ModelFranja, "List Franja Ok");

            DisponibilidadMedicoRepository _InsertDispo = new DisponibilidadMedicoRepository();
            dynamic resultInsert = _InsertDispo.InsertDisponibilidadMedico(ObjetoDisponibilidad(_DoctorsTest, _ModelFranja));

        }

        private Random gen = new Random();
        private DateTime RandomDay()
        {
            DateTime start = new DateTime(2017, 12, 31);
            int range = (start - DateTime.Today).Days;
            return start.AddDays(gen.Next(range));
        }

        private List<DisponibilidadMedico> ObjetoDisponibilidad(List<Doctors> ListDoc, List<FranjaHoraria> ListFranja)
        {
            List<DisponibilidadMedico> ListFinalDispo = new List<DisponibilidadMedico>();
            int min = ListFranja.Select(f => f).Min(p => p.IdFranjaHoraria);
            foreach (var item in ListDoc)
            {
                ListFinalDispo.Add(new DisponibilidadMedico
                {
                    IdFranjaHoraria = gen.Next(ListFranja.Select(f => f).Min(p => p.IdFranjaHoraria), ListFranja.Select(s => s).Max(p => p.IdFranjaHoraria) + 1),
                    IdMedico = item.id,
                    FechaDisponibilidad = RandomDay()
                });
            }

            return ListFinalDispo;
        }


        [TestMethod]
        public void CrearAgendaTest()
        {
            AgendaRepository _Agenda = new AgendaRepository();
            var creaAgenda = _Agenda.AgendaUtomatica("1070951664");
            Assert.IsNotNull("List Medicos Ok");
        }

        [TestMethod]
        public void CrearCitaMedica()
        {
            AsignacionCitasRepository _Agenda = new AsignacionCitasRepository();
            CitasMedica _model = new CitasMedica();
            _model.IdAgenda = 4;
            _model.IdPaciente = 3;
            _model.UsurioRegistra = "1070951664";
            _model.FechaRegistro = DateTime.Now;
            _model.IdEstado = 1;
            var creaAgenda = _Agenda.AsiganacionCitas(_model);
            Assert.IsNotNull("List Medicos Ok");
        }

        /// <summary>
        /// Unit test Modificar Cita Medica 
        /// OOlaya 
        /// 28/04/2017
        /// </summary>
        [TestMethod]
        public void ModificarCita()
        {
            AsignacionCitasRepository _Agenda = new AsignacionCitasRepository();
            var ModificarCita = _Agenda.UpdateCitaMecica("Cancelada",3,"1070951664");
            Assert.IsNotNull("List Medicos Ok");
        }
    }
}
